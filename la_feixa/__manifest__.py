{
    'name': "Odoo La Feixa customizations",
    'version': '12.0.0.0.36',
    'depends': ['easy_my_coop_es','purchase_products_only_from_vendors'],
    'author': "Coopdevs Treball SCCL",
    'website': 'https://coopdevs.org',
    'category': "Cooperative management",
    'description': """
    Odoo La Feixa customizations.
    """,
    "license": "AGPL-3",
    'data': [
        "views/become_company_cooperator_view.xml",
        "views/become_cooperator_view.xml",
        "views/subscription_request_view.xml",
        "views/res_company_view.xml",
        "views/cooperator_thanks_view.xml"
    ],
}
